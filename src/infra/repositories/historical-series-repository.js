
const SequelizeHelper = require('../helpers/sequelize-helper')

module.exports = class HistoricalSeriesRepository {
  constructor () {
    this.CDITax = SequelizeHelper.getModel('CDITax')
    this.Op = SequelizeHelper.getOperators()
  }

  async findAll (config) {
    if (!config || !this.CDITax || !this.CDITax.findAll) {
      return null
    }

    return await this.CDITax.findAll(config)
  }

  async getCDI (dateStart, dateEnd) {
    if (!dateStart || !dateEnd || !this.Op) {
      return null
    }

    try {
      const result = await this.findAll({
        where: {
          date: {
            [this.Op.gte]: dateStart,
            [this.Op.lte]: dateEnd
          }
        },
        order: [
          ['date', 'DESC']
        ]
      })

      return result
    } catch (err) {
      return null
    }
  }
}
