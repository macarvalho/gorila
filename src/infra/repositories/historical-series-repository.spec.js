const HistoricalSeriesRepository = require('./historical-series-repository')

const makeSut = () => {
  const sut = new HistoricalSeriesRepository()

  return {
    sut
  }
}

describe('Historical Series Repository', () => {
  it('should return null if dateStart is not provided in getCDI', async () => {
    const { sut } = makeSut()
    const res = await sut.getCDI()

    expect(res).toBeNull()
  })

  it('should return null if dateEnd is not provided in getCDI', async () => {
    const { sut } = makeSut()
    const res = await sut.getCDI('2021-12-11')

    expect(res).toBeNull()
  })

  it('should return body if all is provided', async () => {
    const { sut } = makeSut()

    sut.findAll = () => {
      return ['test']
    }

    const res = await sut.getCDI('2021-12-11', '2021-12-11')

    expect(res).toEqual(['test'])
  })

  it('should return null if no config is provided to findAll', async () => {
    const { sut } = makeSut()
    const res = await sut.findAll()

    expect(res).toBeNull()
  })
})
