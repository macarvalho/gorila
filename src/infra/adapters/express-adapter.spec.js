const ExpressRouterAdapter = require('./express-adapter')

const makeRequest = () => {
  const request = {
    query: {},
    body: {}
  }

  return request
}

const makeResponse = () => {
  class Response {
    status (status) {
      this.status = status

      return this
    }

    send (data) {
      this.sendData = data

      return this
    }

    json (json) {
      this.json = json

      return this
    }
  }

  return new Response()
}

const makeRouter = () => {
  return {
    route: () => {
      return {
        statusCode: 200,
        body: { test: true }
      }
    }
  }
}

const makeRouterWithError = () => {
  return {
    route: () => {
      return {
        statusCode: 400,
        body: {
          message: 'Test'
        }
      }
    }
  }
}

const makeSut = () => {
  const sut = ExpressRouterAdapter

  return {
    sut
  }
}

describe('Express Adapter', () => {
  it('should return object on call ExpressRouterAdapter as class', async () => {
    const sut = new ExpressRouterAdapter()

    expect(sut).toEqual({})
  })

  it('should return Error if no request is provided', async () => {
    const router = makeRouter()
    const { sut } = makeSut()

    const reqres = sut.adapt(router)
    const result = await reqres()

    expect(result).toBeInstanceOf(Error)
  })

  it('should return Error if no response is provided', async () => {
    const router = makeRouter()
    const { sut } = makeSut()

    const reqres = sut.adapt(router)
    const result = await reqres(makeRequest())

    expect(result).toBeInstanceOf(Error)
  })

  it('should return status code 500 if no HttpResponse is provided', async () => {
    const router = () => {
      return undefined
    }

    const { sut } = makeSut()

    const request = makeRequest()
    const response = makeResponse()

    const reqres = sut.adapt(router)
    await reqres(request, response)

    expect(response.status).toBe(500)
  })

  it('should return response body in json if status code is 200', async () => {
    const router = makeRouter()
    const { sut } = makeSut()

    const request = makeRequest()
    const response = makeResponse()

    const reqres = sut.adapt(router)
    await reqres(request, response)

    expect(response.status).toBe(200)
    expect(response.json).toEqual({
      test: true
    })
  })

  it('should return body message if status code is not 200', async () => {
    const router = makeRouterWithError()
    const { sut } = makeSut()

    const request = makeRequest()
    const response = makeResponse()

    const reqres = sut.adapt(router)
    await reqres(request, response)

    expect(response.status).toBe(400)
    expect(response.json).toEqual('Test')
  })

  it('should return blank message if message is not provided', async () => {
    const router = {
      route: () => {
        return {
          statusCode: 400,
          body: {}
        }
      }
    }

    const { sut } = makeSut()

    const request = makeRequest()
    const response = makeResponse()

    const reqres = sut.adapt(router)
    await reqres(request, response)

    expect(response.status).toBe(400)
    expect(response.json).toEqual('')
  })

  it('should return blank message if body is not provided', async () => {
    const router = {
      route: () => {
        return {
          statusCode: 400
        }
      }
    }

    const { sut } = makeSut()

    const request = makeRequest()
    const response = makeResponse()

    const reqres = sut.adapt(router)
    await reqres(request, response)

    expect(response.status).toBe(400)
    expect(response.json).toEqual('')
  })
})
