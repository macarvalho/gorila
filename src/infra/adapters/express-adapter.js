module.exports = class ExpressRouterAdapter {
  static adapt (router) {
    return async (req, res) => {
      try {
        const httpRequest = {
          query: req.query,
          body: req.body
        }

        const httpResponse = await router.route(httpRequest)
        const body = httpResponse.statusCode === 200 ? httpResponse.body : httpResponse.body ? httpResponse.body.message || '' : ''

        res.status(httpResponse.statusCode).json(body)
      } catch (err) {
        try {
          res.status(500).send('Internal Server Error')
        } catch (err) {
          return new Error('Invalid express response object')
        }
      }
    }
  }
}
