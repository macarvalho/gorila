const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')

const basename = path.basename(__filename)

const env = process.env.ENV || 'production'
const config = require(path.join(__dirname, '/config/config'))[env]

const db = {
  models: {},
  sequelize: new Sequelize(config.database, config.username, config.password, config)
}

fs.readdirSync(path.join(__dirname, '/models'))
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
  })
  .forEach(file => {
    const model = require(path.join(__dirname, '/models', file))(db.sequelize, Sequelize.DataTypes)
    db.models[model.name] = model
  })

Object.keys(db.models).forEach(modelName => {
  if (db.models[modelName].associate) {
    db.models[modelName].associate(db)
  }
})

module.exports = db
