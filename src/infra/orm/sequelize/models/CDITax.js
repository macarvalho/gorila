'use strict'
module.exports = (Sequelize, DataTypes) => {
  const CDITax = Sequelize.define('CDITax', {
    date: DataTypes.DATE,
    lastTradePrice: DataTypes.DECIMAL(6, 2)
  }, {
    timestamps: false
  })

  return CDITax
}
