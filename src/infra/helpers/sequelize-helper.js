const { Op } = require('sequelize')
const { models, sequelize } = require('../orm/sequelize')

module.exports = class SequelizeHelper {
  static async isConnected () {
    try {
      await sequelize.authenticate()
      return true
    } catch (err) { }

    return false
  }

  static getModel (name) {
    return models[name] || null
  }

  static getOperators () {
    return Op
  }
}
