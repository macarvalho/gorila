const SequelizeHelper = require('./sequelize-helper')
const { sequelize } = require('../orm/sequelize')

jest.mock('../orm/sequelize', () => ({
  models: {
    valid_test: {
      isValid: true
    }
  },
  sequelize: {
    authenticate: async () => {
      return true
    }
  }
}))

jest.mock('sequelize', () => ({
  Op: {
    isValid: true
  }
}))

describe('Sequelize Helper', () => {
  afterEach(() => {
    jest.restoreAllMocks()
  })

  it('should return object on call SequelizeHelper as class', async () => {
    const sut = new SequelizeHelper()

    expect(sut).toEqual({})
  })

  it('should return null if don\'t find model', async () => {
    const sut = SequelizeHelper
    const model = sut.getModel('invalid_test')

    expect(model).toBeNull()
  })

  it('should contains getModel method', async () => {
    expect(SequelizeHelper.getModel).toBeTruthy()
  })

  it('should contains getOperators method', async () => {
    expect(SequelizeHelper.getOperators).toBeTruthy()
  })

  it('should return model if it exists', async () => {
    const sut = SequelizeHelper
    const model = sut.getModel('valid_test')

    expect(model).toEqual({
      isValid: true
    })
  })

  it('should return operators of sequelize', async () => {
    const sut = SequelizeHelper
    const model = sut.getOperators()

    expect(model).toEqual({
      isValid: true
    })
  })

  it('should return false if sequelize is offiline', async () => {
    const spy = jest.spyOn(sequelize, 'authenticate').mockImplementation(async () => {
      throw new Error()
    })

    const result = await SequelizeHelper.isConnected()

    expect(result).not.toBeTruthy()
    spy.mockRestore()
  })

  it('should return true if sequelize is online', async () => {
    const result = await SequelizeHelper.isConnected()

    expect(result).toBeTruthy()
  })
})
