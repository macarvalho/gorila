const {
  MissingParamError
} = require('../../presentation/utils/errors')

const UnitPriceDI = require('./unit-price-di')

class HistoricalSeriesRepositorySpy {
  async getCDI (dateStart, dateEnd) {
    this.dateStart = dateStart
    this.dateEnd = dateEnd

    return [
      {
        id: 0,
        date: '2016-12-23',
        lastTradePrice: 13.88
      },
      {
        id: 0,
        date: '2016-12-23',
        lastTradePrice: 13.88
      }
    ]
  }
}

const makeSut = () => {
  const historicalSeriesRepositorySpy = new HistoricalSeriesRepositorySpy()
  const unitPriceDISpy = new UnitPriceDI(historicalSeriesRepositorySpy)

  return {
    sut: unitPriceDISpy,
    historicalSeriesRepositorySpy
  }
}

describe('Calculator UseCase', () => {
  it('should throw if no investmentDate is provided', async () => {
    const { sut } = makeSut()
    const promise = sut.getByDateRange()

    await expect(promise).rejects.toThrow(new MissingParamError('investmentDate'))
  })

  it('should throw if no currentDate is provided', async () => {
    const { sut } = makeSut()
    const promise = sut.getByDateRange('2016-12-26')

    await expect(promise).rejects.toThrow(new MissingParamError('currentDate'))
  })

  it('should throw if no cdbRate is provided', async () => {
    const { sut } = makeSut()
    const promise = sut.getByDateRange('2016-12-26', '2016-10-26')

    await expect(promise).rejects.toThrow(new MissingParamError('cdbRate'))
  })

  it('should call HistoricalSeriesRepository with correct date range', async () => {
    const { sut, historicalSeriesRepositorySpy } = makeSut()
    await sut.getByDateRange('2016-12-26', '2016-10-26', 100)

    expect(historicalSeriesRepositorySpy.dateStart).toBe('2016-12-26')
    expect(historicalSeriesRepositorySpy.dateEnd).toBe('2016-10-26')
  })

  it('should throw if no HistoricalSeriesRepository is provided', async () => {
    const sut = new UnitPriceDI()
    const promise = sut.getByDateRange('2016-12-26', '2016-10-26', 100)

    await expect(promise).rejects.toThrow(new Error('historicalSeriesRepository'))
  })

  it('should throw if no HistoricalSeriesRepository has no getCDI method', async () => {
    class HistoricalSeriesRepositorySpy {}

    const sut = new UnitPriceDI(new HistoricalSeriesRepositorySpy())
    const promise = sut.getByDateRange('2016-12-26', '2016-10-26', 100)

    await expect(promise).rejects.toThrow(new Error('historicalSeriesRepository'))
  })

  it('should returns null if getCDI returns nothing', async () => {
    class HistoricalSeriesRepositorySpy {
      getCDI () {
        return null
      }
    }

    const sut = new UnitPriceDI(new HistoricalSeriesRepositorySpy())
    const result = await sut.getByDateRange('2016-12-26', '2016-10-26', 100)

    await expect(result).toBeNull()
  })

  it('should returns a valid body', async () => {
    const { sut } = makeSut()
    const result = await sut.getByDateRange('2016-12-26', '2016-10-26', 100)

    await expect(result).toEqual([{
      date: '2016-12-23',
      unitPrice: 1000.51591
    }])
  })
})
