const {
  MissingParamError
} = require('../../presentation/utils/errors')

module.exports = class UnitPriceDI {
  constructor (historicalSeriesRepository) {
    this.historicalSeriesRepository = historicalSeriesRepository
  }

  async getByDateRange (investmentDate, currentDate, cdbRate) {
    if (!investmentDate) {
      throw new MissingParamError('investmentDate')
    } else if (!currentDate) {
      throw new MissingParamError('currentDate')
    } else if (!cdbRate) {
      throw new MissingParamError('cdbRate')
    } else {
      try {
        const cdiList = await this.historicalSeriesRepository.getCDI(investmentDate, currentDate)
        if (!cdiList) {
          return null
        }

        const round = (d, n) => parseFloat(d.toFixed(n))

        const result = []
        let acumulated = 1

        for (let i = cdiList.length - 1; i > 0; i--) {
          const tCDI = round(((1 + (cdiList[i].lastTradePrice / 100)) ** (1 / 252)) - 1, 8)
          acumulated *= 1 + tCDI * (cdbRate / 100)

          result.push({
            date: cdiList[i].date,
            unitPrice: round(1000 * acumulated, 5)
          })
        }

        return result.reverse()
      } catch (error) {
        console.log(error)
        throw new Error('historicalSeriesRepository')
      }
    }
  }
}
