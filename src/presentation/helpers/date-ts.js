module.exports = class DateTS {
  constructor (datestr) {
    this.date = new Date(datestr)
  }

  get timestamp () {
    return this.date.getTime()
  }
}
