const DateTS = require('./date-ts')

describe('DateTS', () => {
  it('should transform date format YYYY-MM-DD to timestamp', () => {
    const date = new DateTS('2021-12-01').timestamp

    expect(date).toBe(1638316800000)
  })
})
