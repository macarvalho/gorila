
const HttpResponse = require('./http-response')

const {
  BadRequestError,
  InternalError
} = require('../errors')

const {
  MissingParamError,
  InvalidParamError
} = require('../utils/errors')

describe('HttpResponse', () => {
  test('should return error in MissingParamError even though param is not provided', () => {
    const result = new MissingParamError()

    expect(result).toBeDefined()
  })

  test('should return error in InvalidParamError even though param is not provided', () => {
    const result = new InvalidParamError()

    expect(result).toBeDefined()
  })

  test('should return BadRequestError on BadRequest if param is not provided', () => {
    const result = HttpResponse.badRequest()

    expect(result.statusCode).toBe(400)
    expect(result.body).toEqual(new BadRequestError())
  })

  test('should return null on Ok if param is not provided', () => {
    const result = HttpResponse.ok()

    expect(result.statusCode).toBe(200)
    expect(result.body).toBeNull()
  })

  test('should return body on Ok if body is provided', () => {
    const result = HttpResponse.ok(['test'])

    expect(result.statusCode).toBe(200)
    expect(result.body).toEqual(['test'])
  })

  test('should return specific error on BadRequest if param is provided', () => {
    const result = HttpResponse.badRequest(new InvalidParamError())

    expect(result.statusCode).toBe(400)
    expect(result.body).toEqual(new InvalidParamError())
  })

  test('should return InternalError on serverError', () => {
    const result = HttpResponse.serverError()

    expect(result.statusCode).toBe(500)
    expect(result.body).toEqual(new InternalError())
  })
})
