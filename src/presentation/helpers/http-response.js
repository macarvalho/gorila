const { InternalError, BadRequestError } = require('../errors')

module.exports = class HttpResponse {
  static badRequest (error) {
    return {
      statusCode: 400,
      body: error || new BadRequestError()
    }
  }

  static serverError () {
    return {
      statusCode: 500,
      body: new InternalError()
    }
  }

  static ok (body) {
    return {
      statusCode: 200,
      body: body || null
    }
  }
}
