module.exports = class BadRequestError extends Error {
  constructor () {
    super('Bad Request')
  }
}
