module.exports = class InternalError extends Error {
  constructor () {
    super('Internal Server Error')

    this.name = 'InternalError'
  }
}
