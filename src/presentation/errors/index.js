const InternalError = require('./internal-error')
const BadRequestError = require('./bad-request-error')

module.exports = {
  InternalError,
  BadRequestError
}
