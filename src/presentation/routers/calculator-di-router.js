const HttpResponse = require('../helpers/http-response')
const DateTS = require('../helpers/date-ts')

const {
  MissingParamError,
  InvalidParamError
} = require('../utils/errors')

module.exports = class CalculatorDIRouter {
  constructor ({ unitPriceDI, dateValidator } = { }) {
    this.unitPriceDI = unitPriceDI
    this.dateValidator = dateValidator
  }

  async route (httpRequest) {
    try {
      const { investmentDate, currentDate } = httpRequest.query
      let { cdbRate } = httpRequest.query

      if (!cdbRate && cdbRate !== 0) {
        return HttpResponse.badRequest(new MissingParamError('cdbRate'))
      } else {
        cdbRate = parseFloat(cdbRate)

        if (cdbRate <= 0 || cdbRate >= 300) {
          return HttpResponse.badRequest(new InvalidParamError('cdbRate must be in between 0 and 300'))
        }
      }

      if (!investmentDate) {
        return HttpResponse.badRequest(new MissingParamError('investmentDate'))
      } else if (!this.dateValidator.isValid(investmentDate)) {
        return HttpResponse.badRequest(new InvalidParamError('investmentDate'))
      } else if (!currentDate) {
        return HttpResponse.badRequest(new MissingParamError('currentDate'))
      } else if (!this.dateValidator.isValid(currentDate)) {
        return HttpResponse.badRequest(new InvalidParamError('currentDate'))
      } else if (new DateTS(investmentDate).timestamp > new DateTS(currentDate).timestamp) {
        return HttpResponse.badRequest(new InvalidParamError('investmentDate is greater than currentDate'))
      } else {
        const result = await this.unitPriceDI.getByDateRange(investmentDate, currentDate, cdbRate)
        if (!result) {
          return HttpResponse.serverError()
        }

        return HttpResponse.ok(result)
      }
    } catch (error) {
      return HttpResponse.serverError()
    }
  }
}
