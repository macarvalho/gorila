const CalculatorDIRouter = require('./calculator-di-router')

const {
  InternalError
} = require('../errors')

const {
  MissingParamError,
  InvalidParamError
} = require('../utils/errors')

const makeSut = () => {
  const unitPriceDISpy = makeUnitPriceDI()
  const dateValidatorSpy = makeDateValidator()
  const sut = new CalculatorDIRouter({ unitPriceDI: unitPriceDISpy, dateValidator: dateValidatorSpy })

  return {
    sut,
    unitPriceDISpy,
    dateValidatorSpy
  }
}

const makeUnitPriceDI = () => {
  class UnitPriceDISpy {
    getByDateRange (investmentDate, currentDate, cdbRate) {
      this.investmentDate = investmentDate
      this.cdbRate = cdbRate
      this.currentDate = currentDate

      return ['test']
    }
  }

  const unitPriceDISpy = new UnitPriceDISpy()

  return unitPriceDISpy
}

const makeUnitPriceDIWithError = () => {
  class UnitPriceDISpy {
    getByDateRange () {
      throw new Error()
    }
  }

  const unitPriceDISpy = new UnitPriceDISpy()

  return unitPriceDISpy
}

const makeDateValidator = () => {
  class DateValidatorSpy {
    isValid (date) {
      this.date = date
      return this.invalidDate !== this.date
    }
  }

  const dateValidatorSpy = new DateValidatorSpy()

  return dateValidatorSpy
}

const makeDateValidatorWithError = () => {
  class DateValidatorSpy {
    isValid () {
      throw new Error()
    }
  }

  const dateValidatorSpy = new DateValidatorSpy()

  return dateValidatorSpy
}

describe('Calculator Controller', () => {
  it('should return 500 if no httpRequest is provided', async () => {
    const { sut } = makeSut()

    const httpResponse = await sut.route()
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new InternalError())
  })

  it('should return 500 if httpRequest has no body', async () => {
    const { sut } = makeSut()

    const httpResponse = await sut.route({})
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new InternalError())
  })

  it('should return 400 if no investmentDate is provided', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      query: {
        cdbRate: 100,
        currentDate: '2016-12-26'
      }
    }

    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError('investmentDate'))
  })

  it('should return 400 if no cdbRate is provided', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      query: {
        investmentDate: '2016-11-14',
        currentDate: '2016-12-26'
      }
    }

    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError('cdbRate'))
  })

  it('should return 400 if no currentDate is provided', async () => {
    const { sut } = makeSut()
    const httpRequest = {
      query: {
        cdbRate: 100,
        investmentDate: '2016-11-14'
      }
    }

    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new MissingParamError('currentDate'))
  })

  it('should call UnitPriceDI with correct params', async () => {
    const { sut, unitPriceDISpy } = makeSut()
    const httpRequest = {
      query: {
        cdbRate: 100,
        investmentDate: '2016-11-14',
        currentDate: '2016-12-26'
      }
    }

    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(200)

    expect(unitPriceDISpy.cdbRate).toBe(httpRequest.query.cdbRate)
    expect(unitPriceDISpy.investmentDate).toBe(httpRequest.query.investmentDate)
    expect(unitPriceDISpy.currentDate).toBe(httpRequest.query.currentDate)
  })

  it('should return 500 if UnitPriceDI is not provided', async () => {
    const sut = new CalculatorDIRouter()

    const httpRequest = {
      query: {
        cdbRate: 100,
        investmentDate: '2016-11-14',
        currentDate: '2016-12-26'
      }
    }

    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new InternalError())
  })

  it('should return 500 if DateValidator is not provided', async () => {
    const unitPriceDISpy = makeUnitPriceDI()
    const sut = new CalculatorDIRouter({ unitPriceDI: unitPriceDISpy })

    const httpRequest = {
      query: {
        cdbRate: 100,
        investmentDate: '2016-11-14',
        currentDate: '2016-12-26'
      }
    }

    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new InternalError())
  })

  it('should return 500 if UnitPriceDI has no getByDateRange method', async () => {
    class UnitPriceDISpy {}

    const unitPriceDISpy = new UnitPriceDISpy()
    const dateValidatorSpy = makeDateValidator()
    const sut = new CalculatorDIRouter({ unitPriceDI: unitPriceDISpy, dateValidator: dateValidatorSpy })

    const httpRequest = {
      query: {
        cdbRate: 100,
        investmentDate: '2016-11-14',
        currentDate: '2016-12-26'
      }
    }

    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new InternalError())
  })

  it('should return 500 if DateValidator has no isValid method', async () => {
    class DateValidatorSpy {}

    const unitPriceDISpy = makeUnitPriceDI()
    const sut = new CalculatorDIRouter({ unitPriceDI: unitPriceDISpy, dateValidator: new DateValidatorSpy() })

    const httpRequest = {
      query: {
        cdbRate: 100,
        investmentDate: '2016-11-14',
        currentDate: '2016-12-26'
      }
    }

    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new InternalError())
  })

  it('should return body if everything is ok', async () => {
    const unitPriceDI = makeUnitPriceDI()
    const dateValidatorSpy = makeDateValidator()

    const sut = new CalculatorDIRouter({
      unitPriceDI,
      dateValidator: dateValidatorSpy
    })

    const httpRequest = {
      query: {
        cdbRate: 100,
        investmentDate: '2016-10-14',
        currentDate: '2016-12-26'
      }
    }

    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(200)
    expect(httpResponse.body).toEqual(['test'])
  })

  it('should return 500 if UnitPriceDI throws', async () => {
    const unitPriceDISpy = makeUnitPriceDIWithError()
    const dateValidatorSpy = makeDateValidator()

    const sut = new CalculatorDIRouter({ unitPriceDI: unitPriceDISpy, dateValidator: dateValidatorSpy })

    const httpRequest = {
      query: {
        cdbRate: 100,
        investmentDate: '2016-11-14',
        currentDate: '2016-12-26'
      }
    }

    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new InternalError())
  })

  it('should return 500 if getByDateRange doesn\'t has a body', async () => {
    class UnitPriceDISpy {
      getByDateRange () {
        return null
      }
    }

    const unitPriceDISpy = new UnitPriceDISpy()
    const dateValidatorSpy = makeDateValidator()

    const sut = new CalculatorDIRouter({ unitPriceDI: unitPriceDISpy, dateValidator: dateValidatorSpy })

    const httpRequest = {
      query: {
        cdbRate: 100,
        investmentDate: '2016-11-14',
        currentDate: '2016-12-26'
      }
    }

    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new InternalError())
  })

  it('should return 500 if DateValidator throws', async () => {
    const unitPriceDISpy = makeUnitPriceDI()
    const dateValidatorSpy = makeDateValidatorWithError()

    const sut = new CalculatorDIRouter({
      unitPriceDI: unitPriceDISpy,
      dateValidator: dateValidatorSpy
    })

    const httpRequest = {
      query: {
        cdbRate: 100,
        investmentDate: '2016-11-14',
        currentDate: '2016-12-26'
      }
    }

    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(500)
    expect(httpResponse.body).toEqual(new InternalError())
  })

  it('should return 400 if an invalid investmentDate is provided', async () => {
    const { sut, dateValidatorSpy } = makeSut()
    dateValidatorSpy.invalidDate = '2016-11-14'

    const httpRequest = {
      query: {
        cdbRate: 100,
        currentDate: '2016-12-26',
        investmentDate: '2016-11-14'
      }
    }

    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new InvalidParamError('investmentDate'))
  })

  it('should return 400 if an invalid currentDate is provided', async () => {
    const { sut, dateValidatorSpy } = makeSut()
    dateValidatorSpy.invalidDate = '2016-12-26'

    const httpRequest = {
      query: {
        cdbRate: 100,
        currentDate: '2016-12-26',
        investmentDate: '2016-11-14'
      }
    }

    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toEqual(new InvalidParamError('currentDate'))
  })

  it('should return 400 if an cdbRate out between 0 and 300 is provided', async () => {
    const { sut } = makeSut()

    const cdbRates = [
      0, 300, -10, 301, 320, 1000, -321
    ]

    for (const cdbRate of cdbRates) {
      const httpRequest = {
        query: {
          cdbRate: cdbRate,
          currentDate: '2016-12-26',
          investmentDate: '2016-11-14'
        }
      }

      const httpResponse = await sut.route(httpRequest)
      expect(httpResponse.statusCode).toBe(400)
      expect(httpResponse.body).toBeInstanceOf(InvalidParamError)
    }
  })

  it('should return 400 if investmentDate is greater than currentDate', async () => {
    const { sut } = makeSut()

    const httpRequest = {
      query: {
        cdbRate: 100,
        currentDate: '2016-12-26',
        investmentDate: '2017-11-14'
      }
    }

    const httpResponse = await sut.route(httpRequest)
    expect(httpResponse.statusCode).toBe(400)
    expect(httpResponse.body).toBeInstanceOf(InvalidParamError)
  })
})
