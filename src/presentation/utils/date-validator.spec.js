const DateValidator = require('./date-validator')

describe('Email Validator', () => {
  it('should return true if date is valid', () => {
    const dateValidator = new DateValidator()
    const rows = [
      '2020-12-30',
      '2011-12-02',
      '3000-01-12',
      '0000-01-01'
    ]

    for (const date of rows) {
      const result = dateValidator.isValid(date)

      expect(result).toBe(true)
    }
  })

  it('should return false if date is invalid', () => {
    const dateValidator = new DateValidator()
    const rows = [
      'invalid',
      '2021-13-01',
      '2021-12-40',
      '2020-12-1',
      '01-12-2020',
      '2020/12/01',
      '20-12-01'
    ]

    for (const date of rows) {
      const result = dateValidator.isValid(date)

      expect(result).toBe(false)
    }
  })
})
