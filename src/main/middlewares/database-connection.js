const SequelizeHelper = require('../../infra/helpers/sequelize-helper')

module.exports = async (_req, res, next) => {
  try {
    if (await SequelizeHelper.isConnected()) {
      next()
    } else {
      throw new Error()
    }
  } catch (err) {
    res.status(500).send('Internal Server Error')
  }
}
