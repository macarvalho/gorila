const request = require('supertest')

jest.mock('./database-connection', () => jest.fn((req, res, next) => next()))

describe('Content-Type middleware', () => {
  let app

  beforeEach(() => {
    jest.resetModules()
    app = require('../config/app')
  })

  it('should returns json content-type as default', async () => {
    app.get('/test', (_req, res) => {
      res.send()
    })

    await request(app).get('/test')
      .expect('content-type', /json/)
  })

  it('should return a content-type if declared', async () => {
    app.get('/test', (_req, res) => {
      res.type('xml')
      res.send()
    })

    await request(app).get('/test')
      .expect('content-type', /xml/)
  })
})
