const request = require('supertest')

jest.mock('./database-connection', () => jest.fn((req, res, next) => next()))

describe('CORS middleware', () => {
  let app

  beforeEach(() => {
    jest.resetModules()
    app = require('../config/app')
  })

  it('should enable CORS', async () => {
    app.get('/test', (_req, res) => {
      res.send('')
    })

    const res = await request(app).get('/test')
    expect(res.headers['access-control-allow-origin']).toBe('*')
    expect(res.headers['access-control-allow-methods']).toBe('*')
    expect(res.headers['access-control-allow-headers']).toBe('*')
  })
})
