const request = require('supertest')

jest.mock('./database-connection', () => jest.fn((req, res, next) => next()))

describe('JSON parser middleware', () => {
  let app

  beforeEach(() => {
    jest.resetModules()
    app = require('../config/app')
  })

  it('should parse body as JSON', async () => {
    app.post('/test', (req, res) => {
      res.send(req.body)
    })

    const body = { test: 'test' }
    const res = await request(app).post('/test').send(body)

    expect(res.body).toEqual(body)
  })
})
