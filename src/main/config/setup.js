const helmet = require('helmet')

const cors = require('../middlewares/cors')
const jsonParser = require('../middlewares/json-parser')
const contentType = require('../middlewares/content-type')
const databaseConnection = require('../middlewares/database-connection')

module.exports = (app) => {
  app.use(cors)
  app.use(jsonParser)
  app.use(contentType)
  app.use(databaseConnection)

  app.use(helmet())
}
