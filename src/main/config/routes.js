const express = require('express')
const fg = require('fast-glob')
const path = require('path')

module.exports = (app) => {
  fg.sync('**/src/main/routes/**.js', { ignore: ['**/**.spec.js', '**/**.test.js'] }).forEach((file) => {
    const router = express.Router()

    const mod = require(path.join('../../../', file))
    mod.route(router)

    app.use(mod.path, router)
  })
}
