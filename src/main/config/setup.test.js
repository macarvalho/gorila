const request = require('supertest')

jest.mock('../middlewares/database-connection', () => jest.fn((req, res, next) => next()))

describe('App Setup', () => {
  let app

  beforeEach(() => {
    jest.resetModules()
    app = require('../config/app')
  })

  it('should check if x-powered-by header is disable, returning undefined', async () => {
    app.get('/test', (_req, res) => {
      res.send('')
    })

    const res = await request(app).get('/test')
    expect(res.headers['x-powered-by']).toBeUndefined()
  })
})
