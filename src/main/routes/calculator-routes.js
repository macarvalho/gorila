const { adapt } = require('../../infra/adapters/express-adapter')

const CalculatorDIRouterComposer = require('../composers/calculator-di-compose')

module.exports = {
  path: '/calculator',
  route: (router) => {
    router.get('/di', adapt(new CalculatorDIRouterComposer().compose()))
  }
}
