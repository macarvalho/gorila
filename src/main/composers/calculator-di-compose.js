const HistoricalSeriesRepository = require('../../infra/repositories/historical-series-repository')
const CalculatorDIRouter = require('../../presentation/routers/calculator-di-router')
const DateValidator = require('../../presentation/utils/date-validator')
const UnitPriceDI = require('../../domain/usecases/unit-price-di')

module.exports = class CalculatorDIRouterCompose {
  compose () {
    const historicalSeriesRepository = new HistoricalSeriesRepository()
    const unitPriceDI = new UnitPriceDI(historicalSeriesRepository)
    const dateValidator = new DateValidator()

    return new CalculatorDIRouter({
      unitPriceDI,
      dateValidator
    })
  }
}
