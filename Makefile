up:
	docker-compose -f ./.docker/docker-compose.yaml --project-directory ./ up
down:
	docker-compose -f ./.docker/docker-compose.yaml --project-directory ./ down
logs:
	docker-compose -f ./.docker/docker-compose.yaml --project-directory ./ logs
bash:
	docker-compose -f ./.docker/docker-compose.yaml --project-directory ./ exec api bash