## Produção ##

Você pode testar o projeto em produção acessando [esse link](https://gorilacdi.herokuapp.com/calculator/di?investmentDate=2016-11-14&cdbRate=103.5&currentDate=2016-12-26).

## Características ##

- Feito em NodeJS:14.17.3 (javascript)
- Banco de dados MySQL:8.0.26
- Utilização de containers
- Kubernetes
- Clean Architecture
- Testes

## Requisitos ##

- Docker

## Utilização ##

Clone o repositório e suba os containers pelo docker-compose no diretório do projeto

```bash
docker-compose -f ./.docker/docker-compose.yaml --project-directory ./ up
# ou
make up
```

Aguarde o banco de dados subir e teste sua aplicação na porta 3000

Em decorrência dos requerimentos, apenas a rota /calculator/di está disponível

Você pode testar a aplicação em:

http://localhost:3000/calculator/di?investmentDate=2016-11-14&cdbRate=103.5&currentDate=2016-12-26

## Environment ##

Utilize essas variáveis de ambiente (você pode colocá-las no arquivo .env do diretório raiz do projeto) para ter acesso ao banco de dados
```
# Environment
ENV=production

# MYSQL
MYSQL_USER=marcos
MYSQL_PASSWORD=J8VjkQb$qu&%k2%h
MYSQL_ROOT_PASSWORD=J8VjkQb$qu&%k2%h
MYSQL_DATABASE=dbgorila
MYSQL_HOST=db
```

## Observações ##

As variáveis de ambiente de produção estão expostas com o intuito de agilizar o processo. O banco de dados utilizado foi criado com essa intenção.

## Build & Deployment ##

Você também pode optar por rodar a aplicação em clusters
